UPDATE user as u
    SET u.point_balance = 0
    WHERE u.user_id = 1;

DELETE FROM rel_user
    WHERE user_id = 2;

UPDATE user as u
    SET tier = 'Carbon'
    WHERE u.user_id = 3;
