# Tables are aliased as habit to ensure readability with larger joins.

UPDATE user as u
    SET u.point_balance = 5000
    WHERE u.user_id = 1;

# Template for ensuring we select the correct user.  For example, perhaps we were given an email,
# but need the user_id since the user_id is our foreign key.  Or perhaps it is difficult to visually
# verify that we are indeed changing the correct user_id (e.g. 1000101 vs. 10000101)
INSERT INTO rel_user
    (user_id, rel_lookup, attribute)
    SELECT u.user_id, 'LOCATION', 'USA'
        FROM user u
        WHERE u.user_id = 2;

UPDATE user as u
    SET tier = 'Silver'
    WHERE u.user_id = 3;
