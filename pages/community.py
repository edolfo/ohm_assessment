from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
from queries.community import CommunityQueries


@app.route('/community', methods=['GET'])
def community():

    login_user(User.query.get(1))

    users = CommunityQueries.most_recent(5)

    args = {
            'users': users
    }
    return render_template("community.html", **args)
