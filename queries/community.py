from models._helpers import db

# Read once on app init, then keep query in memory
f = open('./queries/user_select.sql', 'r')
most_recent_query = f.read()
f.close()


class CommunityQueries(object):
    def __init__(self):
        return

    @staticmethod
    def most_recent(num_users):
        query = most_recent_query.format(num_users)
        rows = db.engine.execute(query)
        users = []
        for row in rows:
            users.append({
                'display_name': row[0],
                'tier': row[1],
                'point_balance': row[2]
            })
        return users
